// Book
var Book = {
	title : "...",
	genre : "...",
	author : '...',
	read : false,
	state : null,
	readdate : new Date()
}
//constructor cua book
function Books(title,genre,author,read,state,readdate){
	this.title = title;
	this.genre = genre;
	this.author = author;
	this.read = read;
	this.state = state;
	this.readdate = new Date(readdate);
}

//Booklists
var Booklist = {
	read : 0,
	unread : 0,
	note : ['next to read','reading','the first book read'],
	books : [],

	add(book){
		this.books.push(book);
		if(book.read == true){
			this.read++;
		}else{
			this.unread++;
		}
	},
	finishCurrentBook(){
		var ngayhientai = new Date();
		var d = 0;
		var datemin = this.books[0].readdate;
		for (var i = 0; i < this.books.length; i++) {
			if(this.books[i].readdate < datemin){
				datemin = this.books[i].readdate;
				d = i;
			}
		}
		this.books[d].state = this.note[2];
		for (var i = 0; i < this.books.length; i++) {
			if(this.books[i].readdate.getDate() == ngayhientai.getDate() && this.books[i].readdate.getMonth() == ngayhientai.getMonth() && this.books[i].readdate.getFullYear() == ngayhientai.getFullYear()){
				this.books[i].state = this.note[1];
			}
		}
		for (var i = 0; i < this.books.length; i++) {
			if(this.books[i].state == 'reading'){
				for (var j = 0; j < this.books.length; j++) {
					if(this.books[i].readdate < this.books[j].readdate){
						this.books[j].state = this.note[0];
					}
				}
			}
		}
		for (var i = 0; i < this.books.length; i++) {
			if(this.books[i].state === null){
				this.books[i].state = "Read";
			}
		}
	},
	showInfo(){
		console.log(`
					Number of books read : ${this.read}
					Number of books unread : ${this.unread}
			`);
		for (var i = 0; i < this.books.length; i++) {
			console.log(`
					Title : ${this.books[i].title}
					Genre : ${this.books[i].genre}
					Author : ${this.books[i].author}
					State : ${this.books[i].state}
					Read date: ${this.books[i].readdate}
				`);
		}
	}
}

var book1 = new Books('toan','giao duc','aaa',true,null,'2017 12 25');
var book2 = new Books('ly','giao duc','bbb',true,null,'2017 12 28');
var book3 = new Books('java','giao duc','ccc',true,null,'2018 1 25');
var book4 = new Books('javascript','giao duc','ddd',true,null,new Date());
var book5 = new Books('Jquery','giao duc','fff',false,null,'2018 10 10');
var book6 = new Books('React js','giao duc','eee',false,null,'2018 10 20');

Booklist.add(book2);
Booklist.add(book3);
Booklist.add(book1);
Booklist.add(book4);
Booklist.add(book5);
Booklist.add(book6);

Booklist.finishCurrentBook();
Booklist.showInfo();
// console.log(book1.state);
// console.log(book2.state);
// console.log(book3.state);
// console.log(book4.state);
// console.log(book5.state);
// console.log(book6.state);
